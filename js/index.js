import {gameModel} from './models/gamemodel.js';
import {gameController} from './controllers/gamecontroller.js';
import {gameView} from './views/gameview.js';

console.clear();

//initialize and start a game on document load
window.addEventListener('load', () => {

    const actions = {
        'canvas': document.getElementById('mvc_canvas'),
        'stage_sprite': new PIXI.Sprite(),
        'shapesQtyIndicator': document.getElementById('shapesQty'),
        'surfaceAreaIndicator': document.getElementById('shapesArea'),
        'increaseGravityBtn': document.getElementById('gravityPlusBtn'),
        'decreaseGravityBtn': document.getElementById('gravityMinusBtn'),
        'increaseShapesQtyBtn': document.getElementById('plusBtn'),
        'decreaseShapesQtyBtn': document.getElementById('minusBtn'),
        'gravityModifierInput': document.getElementById('gravityModifier'),
        'shapesQtyModifierInput': document.getElementById('shapesQtyModifier')

    };
    const model = new gameModel([]), view = new gameView(model, actions);
    const controller = new gameController(model, view);
    controller.initializeGame();
});

