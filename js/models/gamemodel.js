import {generateCoords} from "../utils/generators.js";
import {getRandomHexColour, getRandomSidesQuantity} from "../utils/generators.js";
import {shoelaceArea} from "../utils/calculators.js";
import {EventEmitter} from "../eventdispatchers/eventdispatcher.js";


// MVC block
// base class for models

class Shape {

    constructor() {
        this.id = null;
        this.random = false;
        this.sidesQty = 0;
        this.isEllipse = 0;
        this.shapePoints = [];
        this.colour = getRandomHexColour();
        this.pixiObject = null;
        this.coords = [];
        this.deletionMark = 0;
        this.area = 0.0;
        this.lowerBorder = 650
    }

    get pixiObj() {
        return this.pixiObject
    }

    get colr() {
        return this.colour
    }

    get points() {
        return this.shapePoints
    }

    moveShape(gravity) {
        if (this.pixiObject == null || this.coords[1] > this.lowerBorder) {

        } else {
            if (this.pixiObject.interactive === false) {
                this.pixiObject.destroy();
                this.pixiObject = null;
                this.deletionMark = 1;
            } else {
                this.coords[1] += gravity; //moving from up to down
                this.pixiObject.y += gravity;
            }
        }
    }

    generateShape() {
        let graphObject = new PIXI.Graphics();
        let localPos = {x: this.coords[0], y: this.coords[1]};
        //set graphic object for random_shape instance
        this.pixiObject = graphObject;
        this.pixiObj.beginFill(this.colr, 0xFF);
        if (this.sidesQty > 4 || this.sidesQty === 3) {
            let coords = [];
            let coordsList = []; //for area calculation
            let thisIsBezierCurve = (Math.random() > 0.5) ? 1 : 0; //if 1 then draw bezier curve
            if (this.sidesQty === 8) {

                coordsList = generateCoords(this.sidesQty);
                coords = [];

                for (let c = 0; c < coordsList.length; c++) {
                    coords.push(localPos.x + coordsList[c].x);
                    coords.push(localPos.y + coordsList[c].y);
                }
            } else {

                const r = 40;
                for (let i = 0; i <= this.sidesQty; i++) {
                    let x = localPos.x + r * Math.cos((2 * Math.PI * i) / this.sidesQty);
                    let y = localPos.y + r * Math.sin((2 * Math.PI * i) / this.sidesQty);
                    coords.push(Math.floor(x));
                    coords.push(Math.floor(y));
                    let coordXY = {x: Math.floor(x), y: Math.floor(y)};
                    coordsList.push(coordXY);
                }
            }

            if (thisIsBezierCurve === 1) {
                //draw a cloud
                this.pixiObj.moveTo(localPos.x, localPos.y);
                this.pixiObj.bezierCurveTo(localPos.x - 20, localPos.y + 5, localPos.x - 10, localPos.y + 90, localPos.x + 30, localPos.y + 35);
                this.pixiObj.bezierCurveTo(localPos.x + 50, localPos.y + 50, localPos.x + 75, localPos.y + 50, localPos.x + 75, localPos.y + 35);
                this.pixiObj.bezierCurveTo(localPos.x + 140, localPos.y + 35, localPos.x + 85, localPos.y + 20, localPos.x + 105, localPos.y + 10);
                this.pixiObj.bezierCurveTo(localPos.x + 160, localPos.y - 20, localPos.x + 85, localPos.y - 25, localPos.x + 80, localPos.y - 15);
                this.pixiObj.bezierCurveTo(localPos.x + 75, localPos.y - 37, localPos.x + 40, localPos.y - 30, localPos.x + 35, localPos.y - 15);
                this.pixiObj.bezierCurveTo(localPos.x + 15, localPos.y - 37, localPos.x - 10, localPos.y - 30, localPos.x, localPos.y);

                let xarray = [];
                let yarray = [];
                for (let p = 0; p < this.pixiObj.currentPath.points.length; p++) {
                    if (p & 1) {
                        xarray.push(this.pixiObj.currentPath.points[p])
                    } else yarray.push(this.pixiObj.currentPath.points[p])
                }
                coordsList = [];
                for (let crd = 0; crd < xarray.length; crd++) {
                    let coordXY = {x: Math.floor(xarray[crd]), y: Math.floor(yarray[crd])};
                    coordsList.push(coordXY);
                }
                this.area = shoelaceArea(coordsList);
            } else
                this.pixiObj.drawPolygon(coords);
            //calculate area of polygon
            this.area = shoelaceArea(coordsList);
            //hit area
        } else if (this.sidesQty === 4) {
            this.pixiObj.drawRect(localPos.x - 30, localPos.y - 30, 60, 60);
            this.area = 60 * 60; //fixed ares
        } else {
            if (this.isEllipse === 1) {
                this.pixiObj.drawEllipse(localPos.x, localPos.y, 45, 30);
                this.area = Math.floor(Math.PI * 45 * 30);
            } else {
                this.pixiObj.drawCircle(localPos.x, localPos.y, 35);
                this.area = Math.floor(Math.PI * Math.pow(35, 2));
            }
        }
        this.pixiObj.endFill();
        this.pixiObj.interactive = true;
    }
}

/**
 * The Model. Model stores items and notifies
 * observers about changes.
 */
export class gameModel extends EventEmitter {
    constructor(items) {
        super();
        this.app = new PIXI.Application({
            width: 601, height: 600,
            view: document.getElementById("mvc_canvas")
        });
        this.stage = new PIXI.Container();
        this.stageSprite = new PIXI.Sprite();
        this.objectCount = 0;
        this.shapesStack = items || [];
        this.gravity = 1;
        this.shapesGenerationVelocity = 3; //each second
        this.currentSecond = 0;
        this.lowerBorder = 650;
    }

    generateShapeOnCoordinates(x, y) {
        let randomShape = new Shape();
        randomShape.sidesQty = getRandomSidesQuantity();
        randomShape.colour = getRandomHexColour();
        this.objectCount++;
        randomShape.id = this.objectCount;
        randomShape.coords = [x, y];
        if (Math.floor(Math.random() * (1 + 1)) === 1) {
            randomShape.isEllipse = 1;
        }
        randomShape.deletionMark = 0;
        this.addItem(randomShape);
        randomShape.generateShape();
        randomShape.pixiObj.on('click', (e) => this.onShapeClickProcessor(e));
        randomShape.pixiObj.on('tap', (e) => this.onShapeClickProcessor(e));
        this.app.stage.addChild(randomShape.pixiObj);

    }

    setShapesGenerateVelocity(newValue) {
        this.shapesGenerationVelocity = newValue;
        this.emit('modifiedShapesQtyModifier', this.shapesGenerationVelocity);
    }

    increaseShapesGenerateVelocity() {
        this.shapesGenerationVelocity++;
        this.emit('modifiedShapesQtyModifier', this.shapesGenerationVelocity);
    }

    decreaseShapesGenerateVelocity() {
        this.shapesGenerationVelocity--;
        this.emit('modifiedShapesQtyModifier', this.shapesGenerationVelocity);
    }


    setGravity(newValue) {
        this.gravity = newValue;
        this.emit('modifiedGravityModifier', this.gravity);
    }


    increaseGravity() {
        this.gravity++;
        this.emit('modifiedGravity', this.gravity);
        this.emit('modifiedGravityModifier', this.gravity);
    }

    decreaseGravity() {
        this.gravity--;
        this.emit('modifiedGravity', this.gravity);
        this.emit('modifiedGravityModifier', this.gravity);
    }


    addItem(item) {
        this.shapesStack.push(item);
    }

    initializeCanvas() {
        this.stageSprite.interactive = true;
        this.stageSprite.on('click', (e) => this.onClickProcessor(e));
        this.stageSprite.on('tap', (e) => this.onClickProcessor(e));
        this.stageSprite.hitArea = new PIXI.Rectangle(0, 0, 600, 600);
        //Add the sprite to canvas for interaction purposes
        this.app.stage.addChild(this.stageSprite);
        this.app.view.style.display = "block";
        // this.emit('canvasInitialized');
        this.emit('modifiedShapesQtyModifier', this.shapesGenerationVelocity);
        this.emit('modifiedGravityModifier', this.gravity);
    }

    onClickProcessor(e) {
        let localPos = e.data.getLocalPosition(this.stageSprite); //this = stageSprite of gameModel
        this.emit('canvasClick', localPos);
        //add shapes to stageSprite
        this.generateShapeOnCoordinates(localPos.x, localPos.y);
    }

    onShapeClickProcessor(e) {
        e.target.interactive = false;
    }

    calculateTotalArea() {
        //total surface area of elements
        let totalArea = 0;
        for (let iter = 0; iter < this.shapesStack.length; iter++) {
            totalArea += this.shapesStack[iter].area;
        }
        this.emit('totalShapesAreaRefresh', totalArea);
    }

}
